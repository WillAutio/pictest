﻿using DLToolkit.Forms.Controls;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PicTest
{
	public partial class MainPage : ContentPage, INotifyPropertyChanged
    {
        ObservableCollection<PicObj> files;

        private string _picPath { get; set; }
        public string PicPath
        {
            get { return _picPath; }
            set
            {
                _picPath = value;
                RaisePropertyChanged("PicPath");
            }
        }

        public MainPage()
		{
            try
            {
                InitializeComponent();
                FlowListView.Init();
                files = new ObservableCollection<PicObj>();
                PicPath = "";
                this.BindingContext = this;


                takePhoto.Clicked += async (sender, args) =>
                {

                    string path = await takeThePhoto(this);  

                    PicObj po = new PicObj("1", path);
                    files.Add(po);
                    foreach (PicObj s in files)
                    {
                        Debug.WriteLine("listItems.FlowItemsSource:" + s.Name);
                    }
                    image.Source = ImageSource.FromFile(path);
                    PicPath = path;
                    Debug.WriteLine("takePhoto.Clicked - end of method ");
                };
            }
            catch (Exception ex)
            {
                string msg = string.Format("MainPage: constructor {0}. ", ex.ToString());
                Debug.WriteLine(msg);
            }
        }

        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();

                listItems.FlowItemsSource = files;

            }
            catch (Exception ex)
            {
                string msg = string.Format("TakePicturePage: OnAppearing {0}. ", ex.ToString());
                Debug.WriteLine(msg);
            }
        }

        public static async Task<string> takeThePhoto(Page sourcePage)
        {
            MediaFile mediaFile = null;
            string directory = "";
            try
            {
                await CrossMedia.Current.Initialize();

                if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                {
                    await sourcePage.DisplayAlert("No Camera", ":( No camera available.", "OK");
                    return "false";
                }
                Debug.WriteLine("\n takePhoto ");


                string filename = "a.jpg";

                Debug.WriteLine("*** Before CrossMedia.Current.TakePhotoAsync ");


                mediaFile = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                {
                    Name = filename,
                    PhotoSize = PhotoSize.Custom,
                    CustomPhotoSize = 80
                });

                Debug.WriteLine("*** AFTER  CrossMedia.Current.TakePhotoAsync ");
                Debug.WriteLine("File Location:" + mediaFile.Path);

                directory = mediaFile.Path;
            }
            catch (Exception ex)
            {
                string msg = string.Format("TakePictureViewModel: takePhoto {0}. ", ex.ToString());
                Debug.WriteLine("Helpers.takePhoto Exception " + msg);
            }

            Debug.WriteLine("    Done takePhoto ");
            return directory;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
