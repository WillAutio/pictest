﻿namespace PicTest
{
    public class PicObj
    {
        public string ID { get; set; }
        public string Name { get; set; }

        public PicObj()
        {
            ID = "";
            Name = "";
        }

        public PicObj(string id, string name)
        {
            ID = id;
            Name = name;
        }

    }
}

